package antre.items;

import hf.Hf;
import hf.SpecialManager;
import hf.entity.Item;

import vault.ISpec;

class Trophy implements ISpec {
    public var id(default, null): Int;

    public function new(id: Int) {
        this.id = id;
    }

    public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
        specMan.player.lives += 2;
        specMan.game.gi.setLives(specMan.player.pid, specMan.player.lives);
        specMan.game.fxMan.attachShine(item.x, item.y - hf.Data.CASE_HEIGHT * 0.5);
    }

    public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    }
}