package antre.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class NbBads implements IAction {

    public var name(default, null) = Obfu.raw("nbBads");
    public var isVerbose(default, null) = false;

    public function new() {}

    public function run(ctx: IActionContext): Bool {
        var num = ctx.getInt(Obfu.raw("n"));
        for (bad in ctx.getGame().getBadClearList()) {
            num -= bad.fl_kill ? 0 : 1;
        }
        return num == 0;
    }
}
