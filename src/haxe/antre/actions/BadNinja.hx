package antre.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class BadNinja implements IAction {

    public var name(default, null) = Obfu.raw("badNinja");
    public var isVerbose(default, null) = false;

    public function new() {}

    public function run(ctx: IActionContext): Bool {
        var game = ctx.getGame();

        var badId = ctx.getOptInt(Obfu.raw("i")).toNullable();
        var badSid = ctx.getOptInt(Obfu.raw("sid")).toNullable();
        var type = ctx.getInt(Obfu.raw("type"));

        var bads = game.getBadClearList();
        var bad = null;
        if (badId != null) {
            bad = bads[badId - 1];
        } else if (badSid != null) {
            for (b in bads) {
                if (b.scriptId == badSid)
                    bad = b;
            }
        }

        if (bad == null) return false;

        bad.fl_ninFoe = (type == 1);
        bad.fl_ninFriend = (type == 2);

        bad.unstick(); // force the bad to refresh its sticker
        return false;
    }
}
