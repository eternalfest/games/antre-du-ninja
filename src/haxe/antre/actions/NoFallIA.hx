package antre.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class NoFallIA implements IAction {

    public var name(default, null) = Obfu.raw("noFallIA");
    public var isVerbose(default, null) = false;

    public function new() {}

    public function run(ctx: IActionContext): Bool {
        var game = ctx.getGame();
        var x = Std.int(game.flipCoordCase(ctx.getInt(Obfu.raw("x"))));
        var y = ctx.getInt(Obfu.raw("y"));

        game.world.forceFlag({x: x, y: y}, game.root.Data.IA_JUMP_DOWN, false);
        return false;
    }
}
