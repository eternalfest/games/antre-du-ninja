package antre.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class ClearExtras implements IAction {

    public var name(default, null) = Obfu.raw("clearExtras");
    public var isVerbose(default, null) = false;

    public function new() {}

    public function run(ctx: IActionContext): Bool {
        var hf = ctx.getHf();
        var game = ctx.getGame();

        for (bomb in game.getList(hf.Data.BOMB))
            bomb.destroy();

        for (shoot in game.getList(hf.Data.SHOOT))
            shoot.destroy();

        for (item in game.getList(hf.Data.ITEM))
            item.destroy();

        for (player in game.getPlayerList())
            player.unshield();

        return false;
    }
}
