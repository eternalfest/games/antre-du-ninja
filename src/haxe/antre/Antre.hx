package antre;

import patchman.Ref;
import patchman.IPatch;
import etwin.ds.FrozenArray;
import merlin.IAction;
import vault.ISpec;

@:build(patchman.Build.di())
class Antre {

    @:diExport
    public var actions(default, null): FrozenArray<IAction>;

    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    @:diExport
    public var items(default, null): FrozenArray<ISpec>;
    
    public function new(badLink: maxmods.AddLink, patches: Array<IPatch>) {
        this.actions = FrozenArray.of(
            new antre.actions.BadNinja(),
            new antre.actions.NbBads(),
            new antre.actions.NoFallIA(),
            new antre.actions.ClearExtras(),
            badLink.action
        );

        this.items = FrozenArray.of(
            (new antre.items.Trophy(1239): ISpec),
            new antre.items.Trophy(1240),
            new antre.items.Trophy(1241),
            new antre.items.Trophy(1242)
        );

        this.patches = FrozenArray.from(patches.concat([
            // Custom darkness
            Ref.auto(hf.mode.GameMode.startLevel).before(function(hf, self) {
                var id = self.world.currentId;
                self.forcedDarkness = 20 + (id/40) * 30;
            })
        ]));
    }
}
