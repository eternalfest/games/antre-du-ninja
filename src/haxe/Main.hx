import bugfix.Bugfix;
import hf.Hf;
import debug.Debug;
import merlin.Merlin;
import vault.Vault;
import patchman.IPatch;
import patchman.Patchman;
import game_params.GameParams;
import antre.Antre;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  /*
    TODO:
     - disable darkness
     - implement CustomTP mod
  */
  public function new(
    bugfix: Bugfix,
    debug: Debug,
    game_params: GameParams,
    antre: Antre,
    merlin: Merlin,
    vault: Vault,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
