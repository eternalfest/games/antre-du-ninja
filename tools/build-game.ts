const ANCHORS: ReadonlyMap<string, [number, number]> = new Map([
	["trophee_bronze", [13.16, 39.7] as [number, number]],
	["trophee_argent", [13.16, 39.7] as [number, number]],
	["trophee_or", [13.16, 39.7] as [number, number]],
	["trophee_platine", [13.16, 39.7] as [number, number]],
]);

const EXTRA_FRAMES: ReadonlyMap<SpriteRef, ExtraFrameOptions> = new Map([
	[
    "]xUh6" as SpriteRef,
    {
      frames: [
        "trophee_bronze",
        "trophee_argent",
        "trophee_or",
        "trophee_platine"
      ],
    },
  ],
  [
    "0S4B/=S7A",
    {
      frames: [

      ],
    },
  ],
  [
    "0S4B/ nbpb",
    {
      protectedDepths: new Set([1]), // shadow
      frames: [

      ],
    },
  ],
]);

////////////////////////////////////////////////////////////////////////////////

import fs from "fs";
import sysPath from "path";
import { getGamePath } from "@eternalfest/game";
import { emitSwf } from "swf-emitter";
import { DynMovie, swfMerge } from "swf-merge";
import { parseSwf } from "swf-parser";
import { CompressionMethod, Matrix, Movie, Sfixed16P16, SpriteTag, Tag, TagType } from "swf-types";
import { DefineSprite as DefineSpriteTag } from "swf-types/tags/define-sprite";

const PROJECT_ROOT: string = sysPath.join(__dirname, "..");

const INPUT_GAME: string = getGamePath();
const INPUT_ASSETS: string = sysPath.join(PROJECT_ROOT, "build", "assets.swf");
const OUTPUT_GAME: string = sysPath.join(PROJECT_ROOT, "build", "game.swf");

interface ExtraFrameOptions {
  readonly protectedDepths?: ReadonlySet<number>,
  readonly frames: ReadonlyArray<string>,
}

const PIXELS_PER_TWIP: number = 20;

type SpriteRef = string | number;

async function main() {
  const game: Movie = await readSwf(INPUT_GAME);
  let assets: Movie = await readSwf(INPUT_ASSETS);
  assets = applyAnchors(assets, ANCHORS);
  // await writeFile("assets.json", JSON.stringify(assets, null, 2) as any);
  let merged: Movie = swfMerge(game, assets);
  merged = appendFrames(merged, EXTRA_FRAMES);
  // await writeFile("game.json", JSON.stringify(merged, null, 2) as any);
  await writeSwf(OUTPUT_GAME, merged);
}

const IDENTITY_MATRIX: Matrix = {
  scaleX: Sfixed16P16.fromValue(1),
  scaleY: Sfixed16P16.fromValue(1),
  rotateSkew0: Sfixed16P16.fromValue(0),
  rotateSkew1: Sfixed16P16.fromValue(0),
  translateX: 0,
  translateY: 0,
};

function applyAnchors(astMovie: Movie, anchors: ReadonlyMap<string, [number, number]>): Movie {
  const movie = DynMovie.fromAst(astMovie);
  const exports = movie.getNamedExports();
  const definitions = movie.getDefinitions();
  const tags: Tag[] = [...astMovie.tags];
  for (const [linkageName, [posX, posY]] of anchors) {
    const id = exports.get(linkageName);
    if (id === undefined) {
      throw new Error(`ExportNotFound: linkageName = ${JSON.stringify(linkageName)}`);
    }
    const index = definitions.get(id);
    if (index === undefined) {
      throw new Error(`DefinitionNotFound: id = ${id}`);
    }
    const tag: Tag = tags[index];
    if (tag.type !== TagType.DefineSprite) {
      throw new Error(`UnexpectTagType: expected = ${TagType.DefineSprite} (DefineSprite), actual = ${tag.type} (${TagType[tag.type]})`);
    }
    const childTags: ReadonlyArray<SpriteTag> = tag.tags.map((childTag: SpriteTag) => {
      if (childTag.type !== TagType.PlaceObject || childTag.isUpdate) {
        return childTag;
      }
      const oldMatrix: Matrix = childTag.matrix !== undefined ? childTag.matrix : IDENTITY_MATRIX;
      const matrix = {
        ...oldMatrix,
        translateX: oldMatrix.translateX - (posX * PIXELS_PER_TWIP),
        translateY: oldMatrix.translateY - (posY * PIXELS_PER_TWIP),
      };
      return {...childTag, matrix};
    });
    tags[index] = {...tag, tags: childTags};
  }
  return {...astMovie, tags};
}

function appendFrames(astMovie: Movie, extraFrames: ReadonlyMap<SpriteRef, ExtraFrameOptions>) {
  const movie = DynMovie.fromAst(astMovie);
  const exports = movie.getNamedExports();
  const definitions = movie.getDefinitions();
  const tags: Tag[] = [...astMovie.tags];
  for (const [extendedLinkageName, options] of extraFrames) {
    const protectedDepths: ReadonlySet<number> = options.protectedDepths !== undefined
      ? options.protectedDepths
      : new Set();

    const linkageNameParts: readonly string[] = typeof extendedLinkageName === "string"
      ? extendedLinkageName.split("/")
      : [];

    let id: number;
    if (typeof extendedLinkageName === "number") {
      id = extendedLinkageName;
    } else {
      const linkageName: string = linkageNameParts[0];
      const uncheckedId: number | undefined = exports.get(linkageName);
      if (uncheckedId === undefined) {
        throw new Error(`ExportNotFound: linkageName = ${JSON.stringify(linkageName)}`);
      }
      id = uncheckedId;
    }

    const index = definitions.get(id);
    if (index === undefined) {
      throw new Error(`DefinitionNotFound: id = ${id}`);
    }
    const tag: Tag = tags[index];
    if (tag.type !== TagType.DefineSprite) {
      throw new Error(`UnexpectTagType: expected = ${TagType.DefineSprite} (DefineSprite), actual = ${tag.type} (${TagType[tag.type]})`);
    }
    if (linkageNameParts.length <= 1) {
      tags[index] = appendFramesToSprite(tag, options.frames, extendedLinkageName, protectedDepths);
    } else {
      const symbolName: string = linkageNameParts[1];
      let id: number | undefined = undefined;
      for (const childTag of tag.tags) {
        if (childTag.type !== TagType.PlaceObject || childTag.name !== symbolName) {
          continue;
        }
        id = childTag.characterId;
        break;
      }
      if (id === undefined) {
        throw new Error(`CharacterIdNotFound: extendedLinkageName = ${JSON.stringify(extendedLinkageName)}`);
      }
      const index = definitions.get(id);
      if (index === undefined) {
        throw new Error(`DefinitionNotFound: id = ${id}`);
      }
      const spriteTag: Tag = tags[index];
      if (spriteTag.type !== TagType.DefineSprite) {
        throw new Error(`UnexpectTagType: expected = ${TagType.DefineSprite} (DefineSprite), actual = ${tag.type} (${TagType[tag.type]})`);
      }
      tags[index] = appendFramesToSprite(spriteTag, options.frames, extendedLinkageName, protectedDepths);
    }
  }
  return {...astMovie, tags};

  function appendFramesToSprite(
    tag: DefineSpriteTag,
    frames: ReadonlyArray<string>,
    extendedLinkageName: SpriteRef,
    protectedDepths: ReadonlySet<number>,
  ): DefineSpriteTag {
    let frameCount: number = tag.frameCount;
    const childTags: SpriteTag[] = [...tag.tags];
    for (const depth of getUsedDepths(childTags)) {
      if (protectedDepths.has(depth)) {
        continue;
      }
      childTags.push({type: TagType.RemoveObject, depth});
    }
    let minDepth = 0;
    for (const depth of protectedDepths) {
      minDepth = Math.max(minDepth, depth);
    }
    const depth = minDepth + 1;
    let first = true;
    for (const frameLinkageName of frames) {
      const id = exports.get(frameLinkageName);
      if (id === undefined) {
        throw new Error(`ExportNotFound: linkageName = ${JSON.stringify(frameLinkageName)}`);
      }
      console.log(`${extendedLinkageName}[${frameCount}] = ${frameLinkageName}`);
      if (first) {
        first = false;
      } else {
        childTags.push({type: TagType.RemoveObject, depth});
      }
      childTags.push(
        {
          type: TagType.PlaceObject,
          isUpdate: false,
          depth,
          characterId: id,
          className: undefined,
          matrix: undefined,
          colorTransform: undefined,
          ratio: undefined,
          name: undefined,
          clipDepth: undefined,
          filters: undefined,
          blendMode: undefined,
          bitmapCache: undefined,
          visible: undefined,
          backgroundColor: undefined,
          clipActions: undefined,
        },
        {type: TagType.ShowFrame},
      );
      frameCount += 1;
    }

    return {...tag, frameCount, tags: childTags};
  }
}

function getUsedDepths(tags: Iterable<Tag>): Set<number> {
  const depths: Set<number> = new Set();
  for (const tag of tags) {
    if (tag.type === TagType.PlaceObject) {
      depths.add(tag.depth);
    } else if (tag.type === TagType.RemoveObject) {
      depths.delete(tag.depth);
    }
  }
  return depths;
}

async function readSwf(filePath: string): Promise<Movie> {
  const buffer: Buffer = await readFile(filePath);
  return parseSwf(buffer);
}

async function readFile(filePath: string): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, (err, data) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

async function writeSwf(filePath: string, movie: Movie): Promise<void> {
  const bytes: Uint8Array = emitSwf(movie, CompressionMethod.Deflate);
  return writeFile(filePath, bytes);
}

async function writeFile(filePath: string, data: Uint8Array): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath, data, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

main();
