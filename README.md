# L'Antre du Ninja

Clone with HTTPS:
```shell
git clone --recurse-submodules https://gitlab.com/eternalfest/games/antre-du-ninja.git
```

Clone with SSH:
```shell
git clone --recurse-submodules git@gitlab.com:eternalfest/games/antre-du-ninja.git
```
